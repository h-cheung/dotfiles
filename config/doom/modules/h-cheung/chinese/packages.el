;; -*- no-byte-compile: t; -*-
;;; h-cheung/chinese/packages.el


(package! pangu-spacing :disable t)
(package! rime)
(package! fcitx)
(package! ace-pinyin)
(package! pinyinlib)
